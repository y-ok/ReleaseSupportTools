import os
import yaml

CONFIG_DIR = os.path.dirname(os.path.abspath(__file__)) + "/config/"
CONFIG_FILE = "ReleaseConfig.yml"


class ReleaseConfig:
    def __init__(self):
        config_file = CONFIG_DIR + CONFIG_FILE
        self.config = self.load(config_file)

    def load(self, file):
        with open(CONFIG_DIR + CONFIG_FILE, "r") as yml:
            return yaml.safe_load(yml)

    def getSheetConfig(self):
        return self.config["SheetConfig"]

    def getSheetProperty(self):
        return self.config["SheetProperty"]
