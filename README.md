# ReleaseSupportTools

以下の作業を自動化します

- プロジェクトのバンドルファイルの作成
  - `git bundle create YYYYMMDD_プロジェクト名_ver.X.Y.Z.bundle ver.X.Y.Z..master`
- プロジェクトの差分ファイル、コミット履歴の一覧取得し、Excelファイルへ出力
  - `git diff --name-status ver.X.Y.Z..master`
  - `git diff --numstat ver.X.Y.Z..master`

## 実行方法

```bash
python -B main.py <リポジトリパス> <前回リリース時コミットID>
```

## 事前準備

```bash
mkdir package
```

オンライン環境で`openpyxl`を`package`へダウンロードしておく

```bash
$ pip download -d ./package openpyxl
Collecting openpyxl
  Using cached openpyxl-3.0.7-py2.py3-none-any.whl (243 kB)
Collecting et-xmlfile
  Using cached et_xmlfile-1.0.1.tar.gz (8.4 kB)
Saved ./package/openpyxl-3.0.7-py2.py3-none-any.whl
Saved ./package/et_xmlfile-1.0.1.tar.gz
Successfully downloaded openpyxl et-xmlfile
```

```bash
$ tree package
package
├── et_xmlfile-1.0.1.tar.gz
├── openpyxl-2.6.4.tar.gz
├── openpyxl-3.0.7-py2.py3-none-any.whl
└── pip-20.3.4.tar.gz

0 directories, 4 files
```

```bash
tar cfz package.tar.gz package
rm -rf package
```

## pipインストール

```bash
tar xfz package.tar.gz
cd package
easy_install ./pip-20.3.4.tar.gz
```

## 依存ライブラリインストール

```bash
cd package
pip install openpyxl-2.6.4.tar.gz
```

## python 2.7.6インストール

```bash
$ GET_PIP_URL="https://bootstrap.pypa.io/pip/2.7/get-pip.py" CPPFLAGS="-I$(brew --prefix openssl)/include -I$(brew --prefix bzip2)/include -I$(brew --prefix readline)/include -I$(brew --prefix zlib)/include" LDFLAGS="-L$(brew --prefix openssl)/lib -L$(brew --prefix readline)/lib -L$(brew --prefix zlib)/lib -L$(brew --prefix bzip2)/lib" pyenv install 2.7.6
Downloading openssl-1.0.2k.tar.gz...
-> https://pyenv.github.io/pythons/6b3977c61f2aedf0f96367dcfb5c6e578cf37e7b8d913b4ecb6643c3cb88d8c0
Installing openssl-1.0.2k...
Installed openssl-1.0.2k to /Users/okawauchi/.pyenv/versions/2.7.6

python-build: use readline from homebrew
Downloading Python-2.7.6.tgz...
-> https://www.python.org/ftp/python/2.7.6/Python-2.7.6.tgz
Installing Python-2.7.6...
patching file ./Modules/readline.c
patching file ./Lib/site.py
patching file ./Lib/ssl.py
Hunk #2 succeeded at 430 (offset -5 lines).
patching file ./Modules/_ssl.c
Hunk #2 succeeded at 307 (offset -1 lines).
Hunk #3 succeeded at 1809 (offset -3 lines).
patching file ./configure
python-build: use readline from homebrew
python-build: use zlib from xcode sdk
WARNING: The Python bz2 extension was not compiled. Missing the bzip2 lib?
WARNING: The Python sqlite3 extension was not compiled. Missing the SQLite3 lib?
Installing pip from https://bootstrap.pypa.io/pip/2.7/get-pip.py...
Installed Python-2.7.6 to /Users/okawauchi/.pyenv/versions/2.7.6
```
