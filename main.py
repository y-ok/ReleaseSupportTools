# -*- coding: utf-8 -*-
import sys
import Release as rls

if __name__ == "__main__":

    if len(sys.argv) < 3:
        print("[ How to use ]")
        print("python main.py リポジトリ名（フルパス可） 前回リリース時のタグ名（もしくはコミットID）")
        sys.exit()

    PROJECT_NAME = sys.argv[1]
    COMMIT_ID = sys.argv[2]

    release = rls.Release(PROJECT_NAME, COMMIT_ID)
    # バンドルファイル作成
    release.makeBundleFile()
    # リリースノート作成
    release.makeReleaseNote()
