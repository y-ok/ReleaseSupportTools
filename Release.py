# -*- coding: utf-8 -*-
import shutil
import os
import sys
import datetime
import subprocess
import math
from ReleaseConfig import ReleaseConfig
import openpyxl
from openpyxl.styles.alignment import Alignment
from openpyxl.styles import PatternFill

CURRENT_DIR = os.path.dirname(os.path.abspath(__file__)) + "/"
TARGET_DIR = CURRENT_DIR + "target/"


class Release:
    def __init__(self, project_name, from_commit_id, to_commit_id="master"):
        if os.path.exists(TARGET_DIR):
            # targetフォルダを削除して作成
            shutil.rmtree(TARGET_DIR)
            os.makedirs(TARGET_DIR)
        else:
            # targetフォルダが存在しないため、新規作成
            os.makedirs(TARGET_DIR)

        self.from_commit_id = from_commit_id
        self.to_commit_id = to_commit_id

        GIT_DIFF_NUMSTAT_CMD = [
            "git",
            "diff",
            "--numstat",
            from_commit_id + ".." + to_commit_id,
        ]
        GIT_DIFF_NAME_STATUS_CMD = [
            "git",
            "diff",
            "--name-status",
            from_commit_id + ".." + to_commit_id,
        ]
        GIT_DIFF_LOG_CMD = [
            "git",
            "log",
            "--oneline",
            "--pretty=format:%ad %h %an %s",
            "--date=format:%Y-%m-%dT%H:%M:%S",
            from_commit_id + ".." + to_commit_id,
        ]
        self.GIT_CMD_LIST = [
            GIT_DIFF_NUMSTAT_CMD,
            GIT_DIFF_NAME_STATUS_CMD,
            GIT_DIFF_LOG_CMD,
        ]

        # プロジェクト名取得
        self.project_path = project_name
        self.project_name = os.path.basename(project_name)

        # リポジトリへ移動
        if len(os.path.dirname(project_name)) == 0:
            os.chdir(self.project_name)
        else:
            os.chdir(os.path.dirname(project_name) + "/" + self.project_name)

        GIT_DIFF_NUMSTAT_LIST = subprocess.Popen(
            GIT_DIFF_NUMSTAT_CMD, stdout=subprocess.PIPE, stderr=subprocess.PIPE
        )

        GIT_DIFF_NAME_STATUS_LIST = subprocess.Popen(
            GIT_DIFF_NAME_STATUS_CMD,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )

        GIT_DIFF_LOG_LIST = subprocess.Popen(
            GIT_DIFF_LOG_CMD, stdout=subprocess.PIPE, stderr=subprocess.PIPE
        )
        self.GIT_CMD_RESULT = [
            GIT_DIFF_NUMSTAT_LIST,
            GIT_DIFF_NAME_STATUS_LIST,
            GIT_DIFF_LOG_LIST,
        ]

        # スクリプト場所へ移動
        os.chdir(os.path.dirname(os.path.abspath(__file__)))

        releaseConfig = ReleaseConfig()
        self.SheetConfigList = releaseConfig.getSheetConfig()
        self.SheetProperty = releaseConfig.getSheetProperty()

    def makeBundleFile(self):
        """バンドルファイルを生成する"""
        # リポジトリへ移動
        if len(os.path.dirname(self.project_path)) == 0:
            os.chdir(self.project_name)
        else:
            os.chdir(os.path.dirname(self.project_path) + "/" + self.project_name)

        FILE_NAME = self.getBundleFileName(
            self.project_name, self.from_commit_id, self.to_commit_id
        )

        GIT_CREATE_BUNDLE_FILE_CMD = self.gitBundleCreate(
            FILE_NAME, self.from_commit_id, self.to_commit_id
        )

        subprocess.Popen(
            GIT_CREATE_BUNDLE_FILE_CMD, stdout=subprocess.PIPE, stderr=subprocess.PIPE
        )

        # スクリプト場所へ移動
        os.chdir(os.path.dirname(os.path.abspath(__file__)))

    def getBundleFileName(self, project_name, from_commit_id, to_commit_id):
        """バンドルファイル名を生成する

        Args:
            project_name (str): プロジェクト名
            from_commit_id (str): 前回リリースのコミットID
            to_commit_id (str): 今回リリースのコミットID

        Returns:
            tuple: バンドルファイル名
        """
        return (
            TARGET_DIR
            + format(datetime.date.today(), "%Y%m%d")
            + "_"
            + project_name
            + "_"
            + from_commit_id
            + ".."
            + to_commit_id
            + ".bundle"
        )

    def gitBundleCreate(self, file_name, from_commit_id, to_commit_id):
        """バンドルファイルを作成コマンドを生成する

        Args:
            file_name (str): バンドルファイル名
            from_commit_id (str): 前回リリースのコミットID
            to_commit_id (str): 今回リリースのコミットID

        Returns:
            tuple: バンドルファイル生成コマンド
        """
        return (
            "git",
            "bundle",
            "create",
            file_name,
            from_commit_id + ".." + to_commit_id,
        )

    def makeReleaseNote(self):
        """リリースノートを作成する"""
        wb = openpyxl.Workbook()
        for sheetIndex, sheetConfig in enumerate(self.SheetConfigList):
            wb = self.makeSheet(wb, sheetConfig, sheetIndex)

        # 不要シート削除
        wb.remove(wb.worksheets[0])

        # リリースノートファイル名
        FILE_NAME = self.getReleaseNoteFileName(
            self.project_name, self.from_commit_id, self.to_commit_id
        )
        wb.save(FILE_NAME)

    def makeSheet(self, wb, config, sheetIndex):
        """ワークシートを作成する

        Args:
            wb (openpyxl.workbook.workbook.Workbook): 空のワークブック
            config (dict): ワークシートの設定情報
            sheetIndex (int): ワークシートのインデックス

        Returns:
            openpyxl.workbook.workbook.Workbook: ワークブック
        """
        ws = wb.create_sheet(title=config["title"])

        alignmentConfig = self.SheetProperty["alignment"]
        fillConfig = self.SheetProperty["fill"]

        # 枠線削除
        ws.sheet_view.showGridLines = False

        # ヘッダ
        columnList = [
            ws.cell(row=3, column=i + 1) for i in range(len(config["column_title"]))
        ]
        for index, column in enumerate(columnList):
            column.value = config["column_title"][index]
            column.alignment = Alignment(
                horizontal=alignmentConfig["horizontal"],
                vertical=alignmentConfig["vertical"],
                wrap_text=alignmentConfig["wrap_text"],
            )
            column.fill = PatternFill(
                fgColor=fillConfig["fgColor"],
                bgColor=fillConfig["bgColor"],
                fill_type=fillConfig["fill_type"],
            )

        for row_index, data in enumerate(self.GIT_CMD_RESULT[sheetIndex].stdout):
            # データ
            for column_index in range(len(config["column_title"])):
                if sys.version_info.major == 2:
                    if column_index + 1 == len(config["column_title"]) and len(
                        data.strip().split()
                    ) > len(config["column_title"]):
                        ws.cell(
                            row=row_index + 4, column=column_index + 1
                        ).value = " ".join(data.strip().split()[column_index:])
                    else:
                        ws.cell(
                            row=row_index + 4, column=column_index + 1
                        ).value = data.strip().split()[column_index]
                else:
                    if column_index + 1 == len(config["column_title"]) and len(
                        data.strip().decode().split()
                    ) > len(config["column_title"]):
                        ws.cell(
                            row=row_index + 4, column=column_index + 1
                        ).value = " ".join(data.strip().decode().split()[column_index:])
                    else:
                        ws.cell(row=row_index + 4, column=column_index + 1).value = (
                            data.strip().decode().split()[column_index]
                        )
        # 列自動調整
        self.adjust_column(ws)
        ws.cell(row=1, column=1).value = " ".join(self.GIT_CMD_LIST[sheetIndex])
        return wb

    def getReleaseNoteFileName(self, project_name, from_commit_id, to_commit_id):
        """リリースファイル名を取得する

        Args:
            project_name (String): リリース対象のプロジェクト名
            from_commit_id (String): 前回リリース時のコミットID
            to_commit_id (String): 今回リリース時のコミットID

        Returns:
            String: リリースノートファイル名
        """
        return (
            TARGET_DIR
            + format(datetime.date.today(), "%Y%m%d")
            + "_"
            + project_name
            + "_"
            + from_commit_id
            + ".."
            + to_commit_id
            + "_リリースノート.xlsx"
        )

    def adjust_column(self, ws):
        """列幅を自動に調整する

        Args:
            ws (openpyxl.worksheet.worksheet.Worksheet): ワークシート
        """
        for col in ws.columns:
            max_length = 0
            column = col[0].column  # Get the column name
            column = chr(ord("A") - 1 + column)
            for cell in col:
                try:  # Necessary to avoid error on empty cells
                    if len((cell.value).encode("utf-8")) > max_length:
                        max_length = len(cell.value)
                except Exception:
                    pass
            adjusted_width = (max_length + 2) * 1.2
            ws.column_dimensions[column].width = math.ceil(adjusted_width)
